import CodeTableService from '../service/CodeTableService';
import { SessionContext, PageContext, StoreContext ,LocalContext} from 'rainbow-desktop-cache';
import Config from '../config/Config';
import {Util} from 'rainbow-desktop-tools';
import { _ } from 'core-js';
/**
 * @module CodeTableService
 */
module.exports = {

	objKeySort(obj) {
		var newkey = Object.keys(obj).sort();
	  
		var newObj = {};
		for (var i = 0; i < newkey.length; i++) {
			newObj[newkey[i]] = obj[newkey[i]];
		}
		return newObj;
	},

	/**
     * get codetable by codetable id and ConditionMap
     * @param  {Object} param -is a json object
     * @param  {Function} callback -is a option, once get codetable date will run callback function
     * @example
     * import {CodeTableService} from 'rainbow-desktop-codetable';
	 * let param = {
	 * 		CodeTableId:12345,
	 * 		ConditionMap:{"IsDisplay":"Y"},
	 * }
     */
	getCodeTable({ CodeTableId, ConditionMap, CodeTableName, CodeTableUrl }, callback) {
		if (ConditionMap) {
			ConditionMap = this.objKeySort(ConditionMap);
		}
		return new Promise(resolve => {
			const key = this.buildKey({ CodeTableId, ConditionMap, CodeTableName, CodeTableUrl });
			StoreContext.get(key).then((returnCodeTable) => {
				if (returnCodeTable) {
					let codeTableObj = JSON.parse(returnCodeTable);
					let returnData = { codes: codeTableObj.BusinessCodeTableValueList };
					if(codeTableObj['common']){
						returnData['common']=codeTableObj['common'];
					}
					if(Config.defaultProject && ConditionMap){
						let newReturnData=[]
						_.each(returnData.codes,(item)=>{
							if(item.filters&&item.filters.indexOf(ConditionMap[Object.keys(ConditionMap)[0]] )!==-1){
								newReturnData.push(item)
							}
						})
						returnData = newReturnData
					}
					resolve(returnData);
					callback && callback(returnData);
					return;
				} else {
					if(CodeTableUrl){
						return CodeTableService.getCodeTable({ CodeTableId, ConditionMap, CodeTableName,CodeTableUrl }).then(tmpCodeTable => {
							const UserSign = tmpCodeTable['BusinessCodeTable']?tmpCodeTable['BusinessCodeTable']['UserSign']:false;
							const needCache = tmpCodeTable['BusinessCodeTable']['NeedCache'];
							tmpCodeTable = CodeTableService.buildCodeTable(tmpCodeTable);
							delete tmpCodeTable['BusinessCodeTable'];
							delete tmpCodeTable['map'];
							let codeTableObj = { codes: tmpCodeTable.BusinessCodeTableValueList };
							if('Y'==needCache){
								StoreContext.put(key,JSON.stringify(tmpCodeTable)).then((insertCodeTable) => {
									this._handlerUserCodetable(UserSign,CodeTableName,codeTableObj,resolve,callback);
								});
							}else{
								this._handlerUserCodetable(UserSign,CodeTableName,codeTableObj,resolve,callback);
							}
						}).catch((error)=>{
							console.log('codetable set db error,',error);
						});
					}else{
						if(Config.defaultProject){
							let userInfo = SessionContext.get('userInfo')
							let tenantCode= userInfo ? userInfo.firmCode ? userInfo.firmCode : '' : ''
							let lang='en'
							if (LocalContext.get('system_i18nKey').indexOf('_') > -1) {
								lang=LocalContext.get('system_i18nKey').split('_')[0]
							}else if(LocalContext.get('system_i18nKey').indexOf('-') > -1) {
								lang=LocalContext.get('system_i18nKey').split('-')[0]
							}
							return AjaxUtil.call(Config.getDefineCodeTableByNameUrl,
								{
									'codes':[CodeTableName],
									'language':lang,
									'tenantCode':tenantCode
								},
								{'method': 'POST'}).then((tmpCodeTable)=>{
							let returnData = tmpCodeTable.body && tmpCodeTable.body.data && tmpCodeTable.body.data[0] ? tmpCodeTable.body.data[0] :[]
							const UserSign = returnData['UserSign'];
							let obj={
								BusinessCodeTableValueList:[]
							}
							if(returnData.items){
								if(ConditionMap){
									let newReturnData=[]
									_.each(returnData.items,(item)=>{
										if(item.filters&&item.filters.indexOf(ConditionMap[Object.keys(ConditionMap)[0]] )!==-1){
											newReturnData.push(item)
										}
									})
									returnData.items = newReturnData
								}
								obj.BusinessCodeTableValueList = returnData.items
							}
							let codeTableObj = { codes: obj.BusinessCodeTableValueList };
							StoreContext.put(key,JSON.stringify(obj)).then((insertCodeTable) => {
								this._handlerUserCodetable(UserSign,CodeTableName,codeTableObj,resolve,callback);
							});
							});
						}else{
							return AjaxUtil.call(Config.getDefineCodeTableByNameUrl+'?codeTableName='+CodeTableName).then((data)=>{
								const url = data.ServiceName ? Config.getCodeTableByNameUrl.replace(/dd/, data.ServiceName) : Config.getCodeTableByNameUrl;
								AjaxUtil.call(url,{'CodeTableName': data.Name,'ConditionMap':ConditionMap}, {method: 'POST'}).then(tmpCodeTable => {
										const UserSign = tmpCodeTable['BusinessCodeTable']['UserSign'];
										tmpCodeTable = CodeTableService.buildCodeTable(tmpCodeTable);
										delete tmpCodeTable['BusinessCodeTable'];
										delete tmpCodeTable['map'];
										let codeTableObj = { codes: tmpCodeTable.BusinessCodeTableValueList };
										StoreContext.put(key,JSON.stringify(tmpCodeTable)).then((insertCodeTable) => {
											this._handlerUserCodetable(UserSign,CodeTableName,codeTableObj,resolve,callback);
										});
								}).catch((error)=>{
									console.log('codetable set db error,',error);
								});
							});
						}
					}
				}
			}).catch((error)=>{
				console.log('codetable get from db error,',error);
			});
		})
	},

	/**
     * get codetableValue by codetable Name and ConditionMap and codetableKey
     * @param  {Object} param -is a json object
     * @param  {Function} callback -is a option, once get codetable date will run callback function
     * @example
     * import {CodeTableService} from 'rainbow-desktop-codetable';
	 * let param = {
	 * 		CodeTableName:T_ORG,
	 * 		ConditionMap:{"IsDisplay":"Y"},
	 *      CodeTableKey:1
	 * }
     */
	getCodeTableValue({ CodeTableId, ConditionMap, CodeTableName,CodeTableKey }, callback) {
		return new Promise(resolve => {
			this.getCodeTable({ CodeTableId, ConditionMap, CodeTableName,CodeTableKey }).then((data)=>{
						const returnObj = _.find(data.codes,(item)=>{return item.id==CodeTableKey});
						if(returnObj){
							resolve(returnObj.text);
						}else{
							resolve(null);
						}

			});
		})
	},

	checkConditionMap(ConditionMap){
		let flag = [];
		_.each(_.keys(ConditionMap),(key)=>{
			if(!ConditionMap[key]){
				flag.push(key);
			}
		});
		return !_.isEmpty(flag);
	},

	_handlerUserCodetable(UserSign,CodeTableName,codeTableObj,resolve,callback){
		if (UserSign&&UserSign=='Y') {
				const user_codetable = SessionContext.get('USER_CODETABLE');
				const commonData = [];
				_.each(_.keys(user_codetable),(key)=>{
                    if(key==CodeTableName){
                        _.each(user_codetable[key],(item)=>{
                            commonData.push({'id':item['key'],'text':item['value']});
                        })
                    }
                })
				codeTableObj['common'] = commonData;
				resolve(codeTableObj);
				callback && callback(codeTableObj);
		} else {
			resolve(codeTableObj);
			callback && callback(codeTableObj);
		}
	},
	/**
     * get codetable by codetable id and ConditionMap
     * @param  {Object} param -is a json object
     * @param  {Function} callback -is a option, once get codetable date will run callback function
     * @example
     * import {CodeTableService} from 'rainbow-desktop-codetable';
	 * let param = {
	 * 		CodeTableId:12345,
	 * 		ConditionMap:{"IsDisplay":"Y"},
	 * }
     */
	loadCodeTable({ CodeTableId, ConditionMap, CodeTableName }) {
		return new Promise(resolve => {
			const key = this.buildKey({ CodeTableId, ConditionMap, CodeTableName });
			let tmpCodeTable = SessionContext.get(key);
			if (tmpCodeTable) {
				resolve(tmpCodeTable);
				return;
			}
			return CodeTableService.getCodeTable({ CodeTableId, ConditionMap, CodeTableName })
				.then(codeTableObj => {
					const isCache = codeTableObj['BusinessCodeTable']['NeedCache'];
					tmpCodeTable = CodeTableService.buildCodeTable(codeTableObj);
					if (isCache == 'Y') {
						SessionContext.put(key, tmpCodeTable);
					}
					resolve(tmpCodeTable);
				})
		})
	},
	/**
     * get codetable by url
     * @param  {Object} param -is a json object
     * @param  {Function} callback -is a option, once get codetable date will run callback function
     * @example
     * import {CodeTableService} from 'rainbow-desktop-codetable';
	 * const url = 'http://172.25.17.213/restlet/v1/getQuotuionNumber';
	 * const filterExpression = 'QuotuionNumber="001"';
	 *
	 * //url return data format
	 * {
	 * 	.....
	 *  codes:[
	 * 		{'id':'1','text':'shanghai'},
	 * 		{'id':'2','text':'beijing'}
	 * 	]
	 *  ......
	 * }
     */
	fetchCodeTable(url, filterExpression, callback) {
		return new Promise(resolve => {
			return CodeTableService.fetchCodeTable(url)
				.then(tmpCodeTable => {
					let codeTableObj = tmpCodeTable;
					if (filterExpression) codeTableObj = this._filterCodeTable(tmpCodeTable, filterExpression);
					resolve(codeTableObj);
					callback && callback(codeTableObj);
				})
		})
	},

	getCodeTableByNames(codeTableNames) {
		return new Promise(resolve => {
			return CodeTableService.codeTableNames(codeTableNames)
				.then(tmpCodeTable => {
					resolve(tmpCodeTable);
				})
		})
	},

	buildKey({ CodeTableId, ConditionMap, CodeTableName,CodeTableUrl }) {
		if(CodeTableUrl){
			return encodeURI(CodeTableUrl.url); //Util.compile(CodeTableUrl.url);
		}else{
			if (ConditionMap instanceof Array && ConditionMap.length == 0) {
				return 'C_' + CodeTableName;
			}
			return ConditionMap ? 'C_' + CodeTableName + (JSON.stringify(ConditionMap) == '{}'?'':JSON.stringify(ConditionMap)) : 'C_' + CodeTableName;
		}
	},

	_filterCodeTable(codeTable, filterExpression) {
		if (!filterExpression) return codeTable;
		let codes = codeTable.codes;
		let result = codes.filter(code => {
			let conditionFields = code.ConditionFields;
			if (!conditionFields) return true;
			let isEqual = conditionFields.some(condition => {
				let evalStr = '';
				let fields = Object.keys(condition);
				fields.forEach(field => {
					//number, no quotation
					if (!isNaN(condition[field])) evalStr += `let ${field}=${condition[field]};`;
					//not number
					else evalStr += `let ${field}='${condition[field]}';`;
				});
				return eval(`${evalStr};${filterExpression}`);
			});
			return isEqual;
		});
		let tmpCodeTable = CodeTableService.buildCodeTable({ BusinessCodeTableValueList: result });
		return { codes: tmpCodeTable.BusinessCodeTableValueList, map: tmpCodeTable.map };
	},
	handleCodetableList() {
		const arrayMap = PageContext.get('rainbow-codetableName-map');
		if (arrayMap) {
			const arraylist = [];
			arrayMap.forEach((codetable) => {
				arraylist.push(codetable);
			});
			this.getCodeTableByNames(arraylist).then((codetableNames) => {
				let codetabkMap = PageContext.get('rainbow-codetable-map');
				if (codetabkMap) {
					_.each(codetableNames, (names) => {
						const codetableName = names.BusinessCodeTable['Name'];
						const isCache = codeTableObj['BusinessCodeTable']['NeedCache'];
						const conditionMap = names['ConditionMap'] ? names['ConditionMap'] : {};
						const key = codetableName + JSON.stringify(conditionMap);
						const cacheKey = this.buildKey({ 'CodeTableName': codeTableName, 'ConditionMap': conditionMap });
						const tmpCodeTable = CodeTableService.buildCodeTable(names);
						delete tmpCodeTable['BusinessCodeTable'];
						delete tmpCodeTable['map'];
						if (isCache == 'Y') {
							SessionContext.put(key, tmpCodeTable);
						}
						codetabkMap.get(key).forEach((codetableObject) => {
							if (codetableObject.props.io == 'in') {
								if (codetableObject.props.componentType == 'RADIO' || codetableObject.props.componentType == 'CHECKBOX') {
									codetableObject.handlerCheckBoxAndRadio4In(names);
								} else {
									const element = codetableObject.getSelfElement(codetableObject.componentId);
									codetableObject.handlerSelect4In(names, element);
								}
							} else {
								const value = codetableObject.getComponentValue();
								codetableObject.handler4Out(names, value);
							}

						});
					})
				}
				//PageContext.remove("rainbow-codetable-map");
				//PageContext.remove("rainbow-codetableName-map");
			});
		}
	}
}
