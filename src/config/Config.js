import config from 'config';
const _config = sessionStorage.getItem('project_config');
const UI_API_GATEWAY_PROXY_PATH = _config != null ? JSON.parse(_config).UI_API_GATEWAY_PROXY : "";
let _getDefineCodeTableByNameUrl = UI_API_GATEWAY_PROXY_PATH + 'dd/public/codetable/v1/byCodeTableName';
let _getCodeTableByNameUrl = UI_API_GATEWAY_PROXY_PATH + 'dd/public/codetable/v1/data/list/byName';
let _getCodeTableByIdUrl = UI_API_GATEWAY_PROXY_PATH + 'dd/public/codetable/v1/data/codeTableById';
let _getCodeTableByConditionUrl = UI_API_GATEWAY_PROXY_PATH + 'dd/public/codetable/v1/data/condition/in';
let _getCodeTablesByNameUrl = UI_API_GATEWAY_PROXY_PATH + 'dd/public/codetable/v1/codeTableVoList/byNameList';
let _getCommonCodeTablesUrl = UI_API_GATEWAY_PROXY_PATH + 'urp/pub/set/loadObjectByKey?key=MainConfig.SelectConfig..';

const GET_CODETABLE_ID = _config != null ? JSON.parse(_config).GET_CODETABLE_ID :null;
if(GET_CODETABLE_ID){
    _getCodeTableByIdUrl = UI_API_GATEWAY_PROXY_PATH+GET_CODETABLE_ID;
}
if(config.DEFAULT_CODETABLE_API){
    _getDefineCodeTableByNameUrl = UI_API_GATEWAY_PROXY_PATH + config.DEFAULT_CODETABLE_API
}


module.exports = {
    getDefineCodeTableByNameUrl:_getDefineCodeTableByNameUrl,
    getCodeTableByNameUrl: _getCodeTableByNameUrl,
    getCodeTablesByNameUrl: _getCodeTablesByNameUrl,
    getCodeTableByIdUrl: _getCodeTableByIdUrl,
    getCodeTableByConditionUrl: _getCodeTableByConditionUrl,
    getCommonCodeTablesUrl: _getCommonCodeTablesUrl,
    codeTableKeyValue: config.DEFAULT_CODETABLE_KEYVALUE,
    defaultProject: config.DEFAULT_PROJECT
};