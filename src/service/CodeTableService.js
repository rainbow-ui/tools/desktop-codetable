import Config from '../config/Config';
import Constant from '../constant/CodeTableConstant';
import {SessionContext} from 'rainbow-desktop-cache';
module.exports = {

    getCodeTable({ CodeTableId, ConditionMap, CodeTableName, CodeTableUrl }) {
        let url;
        if (CodeTableId && ConditionMap) {
            url = Config.getCodeTableByConditionUrl;
            return AjaxUtil.call(url, { CodeTableId, ConditionMap }, {
                method: "POST"
            });
        } else if (CodeTableId) {
            url = Config.getCodeTableByIdUrl;
            return AjaxUtil.call(url, { CodeTableId }, {
                method: "GET"
            });
        }
        if(CodeTableUrl){
            url = CodeTableUrl.url;
        }else{
            url = Config.getCodeTableByNameUrl;
        }
        const LangId = "";
        return AjaxUtil.call(url, { CodeTableName, ConditionMap, LangId }, {
            method: "POST"
        });
    },

    fetchCodeTable(codetableUrl) {
        if (codetableUrl) {
            return AjaxUtil.call(codetableUrl.url, codetableUrl.param, codetableUrl.setting);
        } else {
            console.error("code table url is null");
        }
    },
    codeTableNames(codetableNames) {
        if (codetableNames) {
            return AjaxUtil.call(Config.getCodeTablesByNameUrl, codetableNames, { "method": "POST" });
        } else {
            console.error("code table name is null");
        }
    },

    buildCodeTable(codes) {
        let map = {}, codeTableKeyValue = Config.codeTableKeyValue;
        if (_.isEmpty(codes.BusinessCodeTableValueList)) {
            codes.BusinessCodeTableValueList = [];
        } else {
            codes.BusinessCodeTableValueList = codes.BusinessCodeTableValueList.map(code => {
                let result = {};
                for (let obj in code) {
                    if (obj === Constant.CODE_TABLE_ID) result[codeTableKeyValue.KEY] = code[obj];
                    else if (obj === Constant.CODE_TABLE_DESCRIPTION) result[codeTableKeyValue.VALUE] = code[obj];
                    else result[obj] = code[obj]
                }
                return result;
            });
            const statusCodeList = SessionContext.get('__CODETABLE_STATUS');
            const returnArray = []; 
            
            _.each(codes.BusinessCodeTableValueList,(code)=>{
                if(statusCodeList&&code.Status){
                    const status = _.find(statusCodeList,(statusCode)=>{
                        return statusCode==code.Status;
                    });
                    if(status){
                        returnArray.push(code);
                    }
                }else{
                    returnArray.push(code);
                }
            });
         
            codes.BusinessCodeTableValueList = returnArray;
        }

        return codes;
    },

    sortCodeTable(codes, sorter) {
        if (sorter) { }
        sorter && sorter.sort(codes.BusinessCodeTableValueList);
        return codes;
    },

    
}